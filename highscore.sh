#!/usr/bin/env bash
set -uo pipefail

trap 'error $? $LINENO' ERR
#trap 'cleanup' EXIT

function echoerr() {
  echo "$*" >&2;
  exit 1;
}

function error(){
  if [ "$1" != "0" ]; then
    echoerr "Error: $1, at line: $2";
    exit 1;
  fi
}

function cleanup(){
  test -d tmp && rm -r tmp;
}

function define_variables(){
  occupation=2512;
  work_dir="tmp";
}



function parse_arguments(){
  while [[ "$#" -gt 0 ]]; do
      case $1 in
          -y|--yrke) if [[ -z "${2:-}" ]]; then echoerr "Illegal argument: Occupation code not specified"; usage; exit 1; else occupation="$2"; fi; shift ;;
          -h|--help)
            usage
            exit 0
            ;;
          *) echo "Illegal arguments: $1"; exit 1 ;;
      esac
      shift
  done
}


# usage function
progname=$(basename $0)
function usage()
{
cat << USAGE

   Usage: $progname [--yrke SSYK_4]

   optional arguments:
     -h, --help             show this help message and exit
     -c, --chaos-test       run in chaos monkey mode

USAGE
}

function set_input(){
  # Set variable input_file to either $1 or /dev/stdin, in case $1 is empty
  # Note that this assumes that you are expecting the file name to operate on on $1
  input_file="${file_name:-/dev/stdin}"
}

function init(){
  define_variables
  parse_arguments "$@"
  set_input
}

function run(){

#mkdir tmp/

curl -s https://data.jobtechdev.se/taxonomy/Kompetensbegrepp_och_relationer_till_kompetensrubrik_och_niv%C3%A5_fyra_i_SSYK-strukturen/Kompetensbegrepp_och_relationer_till_kompetensrubrik_och_niv%C3%A5_fyra_i_SSYK-strukturen.json | jq -r '.data.concepts[].related[].preferred_label' | sort --ignore-case | uniq | cut -d',' -f2 | awk '{$1=$1;print}' | tr '[:upper:]' '[:lower:]' > tmp/taxonomy

cat $input_file |\
         jq --arg occ "$occupation" 'select(.ssyk_lvl4==($occ|tonumber))' |\
         jq -r '.text_enrichments_results.enriched_result.enriched_candidates.competencies[].concept_label' |\
         tr '[:upper:]' '[:lower:]' |\
         sort |\
         uniq -c |\
         sort -r |\
         awk '{$1=$1;print}' |\
         tee tmp/freq |\
         parallel 'echo {} | cut -d" " -f2 | xargs -I {} grep -c --line-regexp {} tmp/taxonomy' > tmp/tax_hit

paste tmp/freq tmp/tax_hit > tmp/highscore
cat tmp/highscore
# clean up
cleanup
}

function main(){
  init "$@"
  run
}

main "$@"
