FROM alpine:20240807

COPY highscore.sh /highscore.sh
RUN apk add --no-cache --upgrade bash jq grep curl coreutils parallel &&\
    chmod a+x /highscore.sh

ENTRYPOINT ["/highscore.sh"]
